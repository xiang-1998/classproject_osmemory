package UesrInterface;

import Adapter.FirstFitAdapter;
import Adapter.NextFitAdapter;
import Adapter.Adapter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;

public class UI extends JFrame
{
    private Container con = new Container();
    private JComboBox modeChooseOptions = new JComboBox();
    private JButton stepDisplay = new JButton("Next Step");
    private JButton autoDisplay = new JButton("Automatically");
    private JButton reset = new JButton("Reset");
    private TextArea board = new TextArea();

    public FirstFitAdapter ffAdapter = new FirstFitAdapter(this);
    public NextFitAdapter nfAdapter = new NextFitAdapter(this);

    private ArrayList<Command> commands;
    private int commandArrayListCnt = 0;

    private static String helpMessage =
            "<html>" +
            "<body>" +
            "<h1>操作系统课程设计</h1>" +
            "<h2>基础信息</h2>" +
            "<ul><li><b>题目：</b>基于空闲分区链的首次适应算法的模拟实现</li>" +
            "    <li><b>小组成员：</b>20167888、20167924、20168066、20168180</li>" +
            "    <li><b>班级：</b>计科1601班</li></ul>" +
            "<h2>基本算法</h2>" +
            "    <h3> 首次适配算法（First Fit，FF）</h3>" +
            "<ul><li>从链首开始顺序查找，直至找到满足要求的内存块</li> <li>释放内存时合并空闲内存块</li> </ul>" +
            "    <h3>循环首次适配算法（Next Fit，NF）</h3>" +
            "<ul><li>从上一次查找的空闲区域的下一块开始顺序查找，直至找到满足要求的内存块</li> <li>释放内存时合并空闲内存块</li> </ul>" +
            "<h2>操作说明</h2>" +
            "<ul><li>顶端菜单可以选择算法</li> <li>中间示意框显示操作指令与结果</li> " +
            "    <li>“Reset”按钮可重新初始化整个流程</li> <li>“Automatically”按钮可以自动演示所有操作</li>" +
            "    <li>“Next Step”操作可逐步运行指令</li>" +
            "    <li>底部将图示内存块状况，白色为空闲内存块</li> </ul>" +
            "</body>" +
            "</html>";

    public UI()
    {
        setTitle("操作系统课程设计——Northeastern University at Qinhuangdao");
        Container container = new Container();
        container.setLayout(new GridLayout(2, 1));
        Container settings = new Container();
        settings.setLayout(new BorderLayout());

        // Tasks Initialization
        commands = new ArrayList<Command>();
        setCommands();

        setAutoDisplay(settings);
        setStepDisplay(settings);
        setBoard(settings);
        setModeChooseOptions(settings);
        setReset(settings);

        container.add(settings);
        con.setSize(1000, 400);
        con.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        container.add(con);

        setContentPane(container);
        setSize(1000, 700);
        setVisible(true);

        // help initialization
        JOptionPane.showMessageDialog(null,
                helpMessage,
                "操作系统课程设计",
                JOptionPane.DEFAULT_OPTION);

    }

    private void setReset(Container settings)
    {
        UI nowUI=this;
        reset.setOpaque(true);
        reset.setBackground(Color.pink);
        reset.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                con.removeAll();
                commandArrayListCnt = 0;
                ffAdapter = new FirstFitAdapter(nowUI);
                nfAdapter = new NextFitAdapter(nowUI);
                appendBoard("Reset Complete!\n");
            }
        });
        settings.add(reset, BorderLayout.SOUTH);
    }

    private void setBoard(Container settings)
    {
        board.setEditable(false);
        board.setFont(new Font("Noto Sans SC",Font.BOLD,32));
        board.append("You choose: First Fit\n");
        settings.add(board, BorderLayout.CENTER);
    }

    private void setStepDisplay(Container settings)
    {
        UI nowUI = this;
        stepDisplay.setOpaque(true);
        //stepDisplay.setBackground(Color.green);
        stepDisplay.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                con.invalidate();
                con.removeAll();
                Adapter adapter;
                if(modeChooseOptions.getSelectedIndex() == 0)
                {
                    adapter = ffAdapter;
                }
                else if(modeChooseOptions.getSelectedIndex() == 1)
                {
                    adapter = nfAdapter;
                }
                else
                {
                    adapter = nfAdapter; // TODO: Maybe more styles?
                }
                try
                {
                    Command current = commands.get(commandArrayListCnt);
                    String operation = current.getState()?"allocates":"releases";
                    board.append("Job " + current.getTag() + ": " + operation + " " + current.getLength() + " memory.\n");
                    commandArrayListCnt++;
                    if (current.getState())
                        adapter.insert(current.getLength(), current.getTag());
                    else
                        adapter.release(current.getTag());
                    for (int i = 0; i < adapter.table.size(); i++)
                    {
                        if (!adapter.table.get(i).getMemoryState())
                        {
                            con.add(new MyCanvas(Color.WHITE,
                                    adapter.table.get(i).getStart(),
                                    adapter.table.get(i).getLength()));
                        }
                        else
                        {
                            ArrayList<Integer> colorArrayList=generateRandomColor();
                            con.add(new MyCanvas(new Color(colorArrayList.get(0), colorArrayList.get(1), colorArrayList.get(2)),
                                    adapter.table.get(i).getStart(),
                                    adapter.table.get(i).getLength()));
                        }
                    }
                }
                catch (RuntimeException err)
                {
                    board.append("Simulation ends.\n");
                    if(modeChooseOptions.getSelectedIndex() == 0)
                    {
                        ffAdapter = new FirstFitAdapter(nowUI);
                    }
                    else if(modeChooseOptions.getSelectedIndex() == 1)
                    {
                        nfAdapter = new NextFitAdapter(nowUI);
                    }
                    commandArrayListCnt = 0;
                }
                con.validate();
            }
        });
        settings.add(stepDisplay, BorderLayout.EAST);
    }

    private void setModeChooseOptions(Container settings)
    {
        UI nowUI = this;
        modeChooseOptions.setOpaque(true);
        //modeChooseOptions.setBackground(Color.CYAN);
        modeChooseOptions.addItem("First Fit");
        modeChooseOptions.addItem("Next Fit");
        modeChooseOptions.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                if (ItemEvent.SELECTED == e.getStateChange())
                {
                    commandArrayListCnt = 0;
                    board.append("You choose: " + e.getItem().toString() + "\n");
                    // select first
                    if(e.getItem().toString().equals("First Fit"))
                    {
                        ffAdapter = new FirstFitAdapter(nowUI);
                    }
                    // select next
                    else if(e.getItem().toString().equals("Next Fit"))
                    {
                        nfAdapter = new NextFitAdapter(nowUI);
                    }
                    con.invalidate();
                    con.removeAll();
                    con.validate();
                }
            }
        });
        settings.add(modeChooseOptions, BorderLayout.NORTH);
    }

    private void setCommands()
    {
        // TODO: Work of Task 1, Task 2, and Task 3
        commands.add(new Command(true, 130, 1));
        commands.add(new Command(true, 60, 2));
        commands.add(new Command(true, 100, 3));
        commands.add(new Command(false, 60, 2));
        commands.add(new Command(true, 200, 4));
        commands.add(new Command(false, 100, 3));
        commands.add(new Command(false, 130, 1));
        commands.add(new Command(true, 140, 5));
        commands.add(new Command(true, 60, 6));
        commands.add(new Command(true, 50, 7));
        commands.add(new Command(false, 60, 6));
    }
    private void setAutoDisplay(Container settings)
    {
        UI nowUI = this;
        // Automatically Presentation
        autoDisplay.setOpaque(true);
        //autoDisplay.setBackground(Color.ORANGE);
        autoDisplay.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                board.append("Automatic presentation begins.\n");
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        super.run();
                        ffAdapter = new FirstFitAdapter(nowUI);
                        nfAdapter = new NextFitAdapter(nowUI);
                        for (int i = 0; i < commands.size() + 1; i++) // yes, add another clicks
                        {
                            stepDisplay.doClick();
                            try
                            {
                                Thread.sleep(1500);
                            }
                            catch (InterruptedException err)
                            {
                                err.printStackTrace();
                            }
                        }
                    }
                };
                t.start();

            }
        });
        settings.add(autoDisplay, BorderLayout.WEST);
    }

    public void appendBoard(String str)
    {
        board.append(str);
    }
    private ArrayList<Integer> generateRandomColor()
    {
        return new ArrayList<Integer> (Arrays.asList((int) Math.round(Math.random() * 255),
                (int) Math.round(Math.random() * 255),
                (int) Math.round(Math.random() * 255)));
    }

}
