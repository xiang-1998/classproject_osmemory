package UesrInterface;

import java.awt.*;

// TODO: Work of Task 4
public class MyCanvas extends Canvas
{
    private Graphics pen;
    private Color color;
    private int wide;
    private int lenth;
    private int start;

    public MyCanvas(Color color, int start, int wide)
    {
        this.wide = (int) (wide * 1.56);
        lenth = wide;
        setSize(this.wide, 350);
        this.color = color;
        this.start = start;
    }

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        pen = g;
        pen.setColor(color);
        pen.fillRect(0, 0, wide, 350);
        pen.setFont(new Font("Noto Sans SC",Font.BOLD,10));
        pen.setColor(Color.orange);
        pen.drawString(String.valueOf(start) + "K", 5, 20);
        pen.drawString(String.valueOf(start + lenth) + "K", wide - 40, 20);
    }

    public Color getColor()
    {
        return color;
    }
}