package UesrInterface;

public class Command
{
    private boolean state; // true->apply
    private int length;
    private int tag;

    Command(boolean state, int length, int tag)
    {
        this.state = state;
        this.length = length;
        this.tag = tag;
    }

    public int getLength()
    {
        return length;
    }
    public boolean getState()
    {
        return state;
    }
    public int getTag()
    {
        return tag;
    }
}
