package Adapter;

import java.util.ListIterator;

import Core.MemoryBlock;
import UesrInterface.UI;


// TODO: Work of Task 4
public class NextFitAdapter extends Adapter
{
    public NextFitAdapter(UI ui, int maxMemorySize)
    {
        super(ui, maxMemorySize);
    }
    public NextFitAdapter(UI ui)
    {
        super(ui);
    }
    private static int pos=0;
    @Override
    public void insert(int length, int tag)
    {
    	boolean done=false;
        //System.out.println("======");
    	
    	ListIterator<MemoryBlock> tableIterator = table.listIterator();
    	while(pos-- != 0) tableIterator.next();    //the position last assigned
    	while(tableIterator.hasNext())
    	{
    		MemoryBlock mBlk = tableIterator.next();
    	   	if(!mBlk.getMemoryState() && mBlk.getLength() >= length)   //the partition is not allocated and the length is satisfactory
    	   	{                                                          //allocation process
            	 MemoryBlock newMBlk = new MemoryBlock(mBlk.getStart(), mBlk.getStart()+length, tag);
            	 mBlk.setStart(mBlk.getStart()+length);
            	 int i=table.indexOf(mBlk);
            	 table.add(i, newMBlk);
            	 tags.add(i);
    	       	 ui.appendBoard("Insertion Accomplished.\n");
    	       	 //System.out.println("Insertion Accomplished.");
    	       	 done = true;
    	       	 pos=i;
    	       	 break;
    	   	}
       	}
    	if(!tableIterator.hasNext() && !done)  //the size of the last free partition is not sufficient
    	{
    		MemoryBlock mBlk = table.getFirst();  //return the first free partition
    		while(pos-- != 0)
    		{
    			int index = table.indexOf(mBlk);
    			if(!mBlk.getMemoryState() && mBlk.getLength() >= length)
        	   	{
                	 MemoryBlock newMBlk = new MemoryBlock(mBlk.getStart(), mBlk.getStart()+length, tag);
                	 mBlk.setStart(mBlk.getStart()+length);
                	 int i=table.indexOf(mBlk);
                	 table.add(i, newMBlk);
                	 tags.add(i);
        	       	 ui.appendBoard("Insertion Accomplished.\n");
        	       	 //System.out.println("Insertion Accomplished.");
        	       	 done = true;
        	       	 pos=i;
        	       	 break;
        	   	}
    			else mBlk = table.get(index+1);
    		}
    	}
    	for(MemoryBlock mBlk : table)  //print the current memory partition
        {
            System.out.printf("%s %d %d %d %d\n", mBlk.getMemoryState()?"True":"False", mBlk.getStart(), mBlk.getEnd(), mBlk.getLength(), mBlk.getTag());
        }
        if(!done)   //allocation failure
        {
            ui.appendBoard("Insertion Failed. Short of memory.\n");
            //System.out.print("Insertion Failed. Short of memory.\n");
        }

    }
}
