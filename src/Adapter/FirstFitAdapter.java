package Adapter;

import Core.MemoryBlock;
import UesrInterface.UI;

public class FirstFitAdapter extends Adapter
{
    public FirstFitAdapter(UI ui, int maxMemorySize)
    {
        super(ui, maxMemorySize);
    }
    public FirstFitAdapter(UI ui)
    {
        super(ui);
    }

    @Override
    public void insert(int length, int tag)
    {
        boolean done=false;
        //System.out.println("======");
        for(MemoryBlock mBlk : table)
        {
            System.out.printf("%s %d %d %d %d\n", mBlk.getMemoryState()?"True":"False", mBlk.getStart(), mBlk.getEnd(), mBlk.getLength(), mBlk.getTag());
            if(!mBlk.getMemoryState() && mBlk.getLength() >= length)
            {
                MemoryBlock newMBlk = new MemoryBlock(mBlk.getStart(), mBlk.getStart()+length, tag);
                mBlk.setStart(mBlk.getStart()+length);
                int i=table.indexOf(mBlk);
                table.add(i, newMBlk);
                tags.add(i);
                ui.appendBoard("Insertion Accomplished.\n");
                //System.out.println("Insertion Accomplished.");
                done = true;
                break;
            }
        }
        if(!done)
        {
            ui.appendBoard("Insertion Failed. Short of memory.\n");
            //System.out.print("Insertion Failed. Short of memory.\n");
        }

    }
}
