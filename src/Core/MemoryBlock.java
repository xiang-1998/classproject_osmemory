package Core;

public class MemoryBlock
{
    private boolean memoryState;
    private int start, end, tag;

    public MemoryBlock ()
    {
        memoryState = false;
        start = end = -1;
        tag = 0;
    }
    public MemoryBlock (int start, int end, int tag)
    {
        memoryState = true;
        this.start = start;
        this.end = end;
        this.tag = tag;
    }

    public int getStart()
    {
        return start;
    }
    public int getEnd()
    {
        return end;
    }
    public int getLength()
    {
        return end-start;
    }
    public int getTag()
    {
        return tag;
    }
    public boolean getMemoryState()
    {
        return memoryState;
    }

    public void setMemoryState(boolean memoryState)
    {
        this.memoryState = memoryState;
    }
    public void setStart(int start)
    {
        this.start = start;
    }
    public void setEnd(int end)
    {
        this.end = end;
    }
    public void setTag(int tag)
    {
        this.tag = tag;
    }
}
